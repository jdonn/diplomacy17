#!/usr/bin/env python3

# ---------------------------
# projects/Diplomacy/Diplomacy.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

# ------------
# Diplomacy_read
# ------------

class Army:

    def __init__(self, name, starting_city, action, target = None, end_result = None):
        self.name = name
        self.starting_city = starting_city
        self.action = action
        self.target = target
        self.end_result = end_result
        if self.action == 'Hold':
            self.target = self.starting_city
        
        
class City:

    def __init__(self, name, armies_attacking = {}, armies_defending = []): 
        self.name = name
        self.armies_attacking = armies_attacking
        self.armies_defending = armies_defending


def diplomacy_read(s):
    """
    reads a single line, with 3 or 4 words each
    s a string
    returns a new Army object with the information from the line
    """
    a = s.split()
    tuple(a)
    new_army = Army(*a)
    return new_army


# ------------
# Diplomacy_eval
# ------------

def pre_conditions(f):
    '''
    Tests preconditions for inputs to diplomacy_eval
    '''
    def g(armies):
        assert type(armies) == list
        assert len(armies) > 0
        for army in armies:
            assert len(army.name) == 1
            assert len(army.starting_city) > 1
            if army.action == 'Move':
                assert len(army.target) > 1
            elif army.action == 'Support':
                assert len(army.target) == 1
        return f(armies)
    return g

def post_conditions(f):
    '''
    Tests postconditions for outputs from diplomacy_eval
    '''
    def g(armies):
        v = f(armies)
        assert type(v) == list
        assert len(v) > 0
        return v
    return g


@pre_conditions
@post_conditions
def diplomacy_eval(armies):
    """
    takes armies, processes their actions, prints result
    """
    # creating and populating a list of cities
    cities = []
    for army in armies:
        if army.action == 'Hold':
            new_city = City(army.starting_city, armies_attacking = {}, armies_defending = [army.name])
            cities.append(new_city)
                       
                
        elif army.action == 'Move':
            new_city = City(army.starting_city, armies_attacking = {}, armies_defending = [])
            cities.append(new_city)
            for city in cities:
                if city.name == army.target:
                    city.armies_attacking[army.name] = []
                    break
            
                        
        else: # army.action == 'Support'
            new_city = City(army.starting_city, armies_attacking = {}, armies_defending = [])
            cities.append(new_city)
            
            # searching for the correct army object
            for potential_ally_army in armies:
                if army.target == potential_ally_army.name:
                    army_to_support = potential_ally_army
                    break
            # searching for correct city object
            if army_to_support.action == 'Hold':
                for city in cities:
                    if army_to_support.starting_city == city.name:
                        city.armies_defending.append(army.name)
                        break
                        
            elif army_to_support.action == 'Move':
                for city in cities:
                    if army_to_support.target == city.name:
                        city.armies_attacking[army.target].append(army.name)
                        break
        
            
    # invalidate supports here, in case an army that's trying to support another army is being attacked in its starting city:
    for army in armies:
        if army.action == 'Support':
            # search for correct starting city object (the home city of the current army)
            for city in cities:
                if army.starting_city == city.name:
                    starting_city = city
                    if len(starting_city.armies_attacking) > 0:
                        # if supporting army's home city is being attacked, stop the army from attacking the city, 
                        # and make them defend their home city; edit city's lists here:
                        # This line finds the army that the current army is supporting:
                        for ally_army in armies:
                            if army.target == ally_army.name:
                                # finds the target city of the army that the current army is supporting
                                for city in cities:
                                    if city.name == ally_army.target:
                                        if ally_army.action == 'Move':
                                            city.armies_attacking[army.target].remove(army.name)
                                            break
                                        elif ally_army.action == 'Hold':
                                            city.armies_defending.remove(army.name)
                                            break
                                starting_city.armies_defending.append(army.name)
                                break
                    break
    
    # creating results
    for city in cities:
        # if this city is being attacked
        if city.armies_attacking:
            strengths = {}
            for faction in city.armies_attacking:
                strengths[faction] = len(city.armies_attacking[faction]) + 1
            strengths[city.armies_defending[0]] = len(city.armies_defending)
            
            # iterates through each faction, finds the strongest one(s)
            strongest_factions = []
            for faction in strengths:
                if not strongest_factions:
                    strongest_factions = [faction]
                else:
                    if strengths[faction] == strengths[strongest_factions[0]]:
                        strongest_factions.append(faction)
                    elif strengths[faction] > strengths[strongest_factions[0]]:
                        strongest_factions = [faction]
            
            # everyone dies
            if len(strongest_factions) > 1:
                for faction in strengths:
                    current_army = find_army_object(armies, faction)
                    current_army.end_result = '[dead]'
            
            # case of where there is one winner
            elif len(strongest_factions) == 1:
                for faction in strengths:
                    current_army = find_army_object(armies, faction)
                    current_army.end_result = '[dead]'
                winning_army = find_army_object(armies, strongest_factions[0])
                if winning_army.action == 'Hold':
                    winning_army.end_result = winning_army.starting_city
                elif winning_army.action == 'Move':
                    winning_army.end_result = winning_army.target
    
    # for remaining armies: supporting armies return home
    for army in armies:
        if army.end_result == None:
            army.end_result = army.starting_city
        
    return armies
    
    
def find_army_object(armies, army_name):
    '''
    Helper function that finds the correct army function in the given armies list
    '''
    for army in armies:
        if army.name == army_name:
            return army
            
# -------------
# Diplomacy_print
# -------------

def diplomacy_print(armies, w):
    '''
    Prints armies and battle end results in proper format
    '''
    for army in armies:
        string = '{} {}\n'.format(army.name, army.end_result)
        w.write(string)

# -------------
# Diplomacy_solve
# -------------


def diplomacy_solve(r, w):
    """
    r a reader
    w a writer
    """
    armies = []
    for s in r:
        army = diplomacy_read(s)
        armies.append(army)
    
    armies = diplomacy_eval(armies)
    diplomacy_print(armies, w)
